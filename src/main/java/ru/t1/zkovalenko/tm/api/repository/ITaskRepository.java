package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task add(Task project);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
