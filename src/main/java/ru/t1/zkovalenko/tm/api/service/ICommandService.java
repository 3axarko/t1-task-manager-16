package ru.t1.zkovalenko.tm.api.service;

import ru.t1.zkovalenko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalControls();

}
