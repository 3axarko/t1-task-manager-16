package ru.t1.zkovalenko.tm.api.model;

import ru.t1.zkovalenko.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
